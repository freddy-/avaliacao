Aplicação para listar cards dos países possibilitando filtrar por Nome.

Tecnologias utilizadas:
* React
* Redux
* Apollo GraphQL
* Material UI

Para iniciar a aplicação:
```
$ yarn start
```

Para executar os testes:
```
$ yarn test
```

*Observação*: 
Tentei utilizar o `filter name_contains` da API de GraphQL mas aparentemente existe um bug na biblioteca utilizada no servidor de exemplo(neo4j-graphql-js) no qual o filter não funciona.