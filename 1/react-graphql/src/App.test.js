import React from "react";
import { render } from "@testing-library/react";
import { MockedProvider } from "@apollo/client/testing";
import { Provider } from "react-redux";
import { act } from "react-dom/test-utils";
import store from "./app/store";
import App from "./App";
import { SEARCH_COUNTRIES_BY_NAME } from "./features/countries/CountriesHook";

test("renderiza mensagem Carregando", async () => {
  const { getByText } = render(
    <MockedProvider mocks={[]} addTypename={false}>
      <Provider store={store}>
        <App />
      </Provider>
    </MockedProvider>
  );

  expect(getByText(/Pesquisa de Países com GraphQL/i)).toBeInTheDocument();
  expect(getByText(/Carregando/i)).toBeInTheDocument();
});


test("renderiza o card com o resultado da consulta", async () => {
  const mock = {
    request: {
      query: SEARCH_COUNTRIES_BY_NAME,
      variables: {},
    },
    result: {
      data: {
        Country: [
          {
            name: "Brazil",
            capital: "Brasília",
            flag: { emoji: "🇧🇷", __typename: "Flag" },
            __typename: "Country",
          },
        ],
      },
    },
  };

  const { getByText } = render(
    <MockedProvider mocks={[mock]} addTypename={false}>
      <Provider store={store}>
        <App />
      </Provider>
    </MockedProvider>
  );

  await act(async () => {
    await new Promise((resolve) => setTimeout(resolve, 0));
  });

  expect(getByText(/Pesquisa de Países com GraphQL/i)).toBeInTheDocument();
  expect(getByText(/Brazil/i)).toBeInTheDocument();
  expect(getByText(/Brasília/i)).toBeInTheDocument();
});


test("renderiza mensagem de nenhum resultado encontrado", async () => {
  const mock = {
    request: {
      query: SEARCH_COUNTRIES_BY_NAME,
      variables: {},
    },
    result: {
      data: {
        Country: [],
      },
    },
  };

  const { getByText } = render(
    <MockedProvider mocks={[mock]} addTypename={false}>
      <Provider store={store}>
        <App />
      </Provider>
    </MockedProvider>
  );

  await act(async () => {
    await new Promise((resolve) => setTimeout(resolve, 0));
  });

  expect(getByText(/Pesquisa de Países com GraphQL/i)).toBeInTheDocument();
  expect(getByText(/Nenhum resultado encontrado/i)).toBeInTheDocument();
});
