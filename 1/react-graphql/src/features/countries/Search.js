import React, { useState } from "react";
import { useSelector } from "react-redux";
import {
  Paper,
  Typography,
  FormControl,
  InputLabel,
  OutlinedInput,
  InputAdornment,
  IconButton,
} from "@material-ui/core";
import { selectLoading } from "./countriesSlice";
import SearchIcon from "@material-ui/icons/Search";
import { useCountries } from "./CountriesHook";
import "./Search.css";

export default function Search() {
  const [inputValue, setInputValue] = useState("");
  const [filterName, setFilterName] = useState("");
  const loading = useSelector(selectLoading);
  useCountries(filterName);

  const handleOnKeyDown = ({ code }) => {
    if (code === "Enter") setFilterName(inputValue);
  };

  return (
    <Paper className="container">
      <Typography align="center" variant="h4" gutterBottom>
        Pesquisa de Países com GraphQL
      </Typography>

      <FormControl fullWidth variant="outlined">
        <InputLabel htmlFor="search-bar">Pesquisar</InputLabel>
        <OutlinedInput
          id="search-bar"
          value={inputValue}
          onKeyDown={handleOnKeyDown}
          onChange={(e) => setInputValue(e.target.value)}
          endAdornment={
            <InputAdornment position="end">
              <IconButton
                disabled={loading}
                onClick={() => setFilterName(inputValue)}
                onMouseDown={(event) => event.preventDefault()}
                edge="end"
              >
                <SearchIcon />
              </IconButton>
            </InputAdornment>
          }
          labelWidth={70}
        />
      </FormControl>
    </Paper>
  );
}
