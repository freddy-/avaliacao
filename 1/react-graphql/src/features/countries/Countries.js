import React from "react";
import { useSelector } from "react-redux";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Grid from "@material-ui/core/Grid";
import Search from "./Search";
import {
  selectLoading,
  selectError,
} from "./countriesSlice";
import "./Countries.css";
import { useCountries } from './CountriesHook';

export function Countries() {
  const loading = useSelector(selectLoading);
  const error = useSelector(selectError);
  const countries = useCountries();

  const renderError = () => {
    return <Typography align="center">Falha ao carregar países</Typography>;
  };

  const renderLoading = () => {
    return <Typography align="center">Carregando...</Typography>;
  };

  const renderCountries = () => {
    if (!countries?.length) {
      return (
        <Typography align="center">Nenhum resultado encontrado</Typography>
      );
    }

    return (
      <Grid container spacing={3}>
        {countries.map((country) => renderCard(country))}
      </Grid>
    );
  };

  const renderCard = ({ name, capital, flag }) => {
    return (
      <Grid item xs={12} sm={4} key={name}>
        <Card className="card">
          <CardContent>
            <Typography variant="h2" align="center">
              {flag.emoji}
            </Typography>
            <Typography variant="subtitle2" color="textSecondary">
              Nome:
            </Typography>
            <Typography gutterBottom>{name}</Typography>
            <Typography variant="subtitle2" color="textSecondary">
              Capital:
            </Typography>
            <Typography>{capital}</Typography>
          </CardContent>
        </Card>
      </Grid>
    );
  };

  return (
    <Container className="container">
      <Search />

      <Paper elevation={2} className="resultContainer">
        {loading && renderLoading()}
        {error && renderError()}
        {!loading && !error && renderCountries()}
      </Paper>
    </Container>
  );
}
