import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { gql, useQuery } from "@apollo/client";
import {
  setCountries,
  selectCountries,
  setError,
  setLoading,
} from "./countriesSlice";

export const SEARCH_COUNTRIES_BY_NAME = gql`
  query Country($name: String) {
    Country(name: $name) {
      name
      capital
      flag {
        emoji
      }
    }
  }
`;

export function useCountries(name) {
  let variables = name ? { name } : {};
  
  const dispatch = useDispatch();
  const countries = useSelector(selectCountries);

  const { loading, error, data } = useQuery(SEARCH_COUNTRIES_BY_NAME, {
    variables,
  });

  useEffect(() => {
    dispatch(setLoading(!data));
    dispatch(setCountries(data?.Country));
  }, [dispatch, data, loading])

  useEffect(() => {
    dispatch(setError(error));
  }, [dispatch, error]);

  return countries;
}
