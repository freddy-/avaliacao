import { createSlice } from '@reduxjs/toolkit';

export const countriesSlice = createSlice({
  name: 'countries',
  initialState: {
    value: [],
    loading: false,
    error: false,
  },
  reducers: {
    setLoading: (state, action) => {
      return {
        ...state,
        loading: action.payload,
        error: false,
      }
    },
    setError: (state, action) => {
      return {
        ...state,
        error: !!action.payload,
      }
    },
    setCountries: (state, action) => {
      return { 
        ...state, 
        value: action.payload,
        error: false,
      };
    },
  },
});

export const { setCountries, setLoading, setError } = countriesSlice.actions;

export const selectCountries = state => state.countries.value;
export const selectLoading = state => state.countries.loading;
export const selectError = state => state.countries.error;

export default countriesSlice.reducer;
