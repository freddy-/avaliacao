import React from 'react';
import { Countries } from './features/countries/Countries';

function App() {
  return (
    <div>      
      <Countries />
    </div>
  );
}

export default App;
