Este repositório contém a solução dos 3 exercícios propostos. Cada exercício está em sua respectiva pasta com um README relacionado a ele.

[Exercício 1 – React && Graphql](1/react-graphql/README.md)

[Exercício 2 – Algoritmos](2/exercicio2/README.md)

[Exercício 3 – API](3/tax/README.md)