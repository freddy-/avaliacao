
O projeto foi criado utilizando Java 11 e JaCoCo para validar cobertura dos testes.

Foram implementadas validações para quando não é informado a quantidade minima de parâmetros (disco e um arquivo) e também quando o arquivo é zero ou excede o tamanho do disco.

Para executar o projeto basta executar o comando abaixo. O primeiro parâmetro é o tamanho do disco e os subsequentes são os arquivos.
```
$ mvn compile exec:java \
  -Dexec.arguments="100,20,30,50,10,15"

...
Resultado: 3 (discos)
...
```

Para executar os testes e verificar a cobertura (a cobertura minima foi configurada para 90%):
```
$ mvn verify
```
Será gerado um arquivo html contendo o detalhamento da cobertura:
```
target/site/jacoco/index.html
```

Observação:
Acredito que tenha um erro no exemplo do exercicio, pois de acordo com as regras propostas o resultado deveria ser 3 discos e não 2.
Também percebi que não existe vírgula entre o 30 e 50 no exemplo, dependendo de como é implementado o parse estes valores poderiam ter sido considerados como um só.