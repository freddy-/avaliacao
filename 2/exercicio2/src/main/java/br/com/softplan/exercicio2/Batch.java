package br.com.softplan.exercicio2;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class Batch {
    int tapeSize;
    List<Integer> fileSizes;
}
