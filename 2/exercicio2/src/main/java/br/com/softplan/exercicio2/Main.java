package br.com.softplan.exercicio2;

import java.util.Comparator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        var batch = validateAndCreateBatch(args);
        var tapeCount = getMinimumTapeCount(batch);
        System.out.println("Resultado: " + tapeCount + " (discos)");
    }

    private static Batch validateAndCreateBatch(String[] args) {
        if (args == null || args.length < 2) {
            throw new IllegalArgumentException("Necessário informar tamanho do disco e pelo menos um arquivo.");
        }

        var tapeSize = Integer.parseInt(args[0]);
        var fileSizes = Stream.of(args)
            .skip(1)
            .map(Integer::parseInt)
            .peek(file -> {
                if (file > tapeSize) {
                    throw new IllegalArgumentException("Arquivo não pode ser maior que o disco.");
                }
                if (file <= 0) {
                    throw new IllegalArgumentException("Arquivo não pode ser vazio.");
                }
            })
            .collect(Collectors.toList());

        return new Batch(tapeSize, fileSizes);
    }

    private static int getMinimumTapeCount(Batch batch) {
        int tapeCount = 0;
        var files = batch.getFileSizes().stream()
            .sorted(Comparator.reverseOrder())
            .collect(Collectors.toList());

        while (files.size() > 0) {
            tapeCount++;

            int biggerFile = files.get(0);
            int freeSpace = batch.getTapeSize() - biggerFile;

            var smallerFile = files.stream()
                .skip(1)
                .filter(f -> f <= freeSpace)
                .findFirst()
                .orElse(0);

            if (smallerFile > 0) {
                files = files.stream().skip(2).collect(Collectors.toList());
            } else {
                files = files.stream().skip(1).collect(Collectors.toList());
            }
        }

        return tapeCount;
    }
}
