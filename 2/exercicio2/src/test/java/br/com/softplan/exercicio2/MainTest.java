package br.com.softplan.exercicio2;

import org.junit.Before;
import org.junit.Test;

import org.assertj.core.api.Assertions;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class MainTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @Test
    public void getMinimumTapeCount_deveRetornarFalha_quandoNaoEnviarParametros() {
        Assertions.assertThatExceptionOfType(IllegalArgumentException.class)
            .isThrownBy(() -> Main.main(null))
            .withMessage("Necessário informar tamanho do disco e pelo menos um arquivo.");

        Assertions.assertThatExceptionOfType(IllegalArgumentException.class)
            .isThrownBy(() -> Main.main(new String[]{"100"}))
            .withMessage("Necessário informar tamanho do disco e pelo menos um arquivo.");
    }

    @Test
    public void getMinimumTapeCount_deveRetornarFalha_quandoEnviarArquivoMaiorQueDisco() {
        Assertions.assertThatExceptionOfType(IllegalArgumentException.class)
            .isThrownBy(() -> Main.main(new String[]{"100","500"}))
            .withMessage("Arquivo não pode ser maior que o disco.");
    }

    @Test
    public void getMinimumTapeCount_deveRetornarFalha_quandoEnviarArquivoVazio() {
        Assertions.assertThatExceptionOfType(IllegalArgumentException.class)
            .isThrownBy(() -> Main.main(new String[]{"100","0"}))
            .withMessage("Arquivo não pode ser vazio.");
    }

    @Test
    public void getMinimumTapeCount_deveRetornarQuatroDiscos_quandoEnviarParametrosCorretos() {
        Main.main(new String[]{"100", "20", "30", "50", "10", "15", "100"});
        Assertions.assertThat(outContent.toString())
            .isEqualTo("Resultado: 4 (discos)\n");
    }

    @Test
    public void getMinimumTapeCount_deveRetornarTresDiscos_quandoEnviarParametrosCorretos() {
        Main.main(new String[]{"100", "20", "30", "50", "10", "15"});
        Assertions.assertThat(outContent.toString())
            .isEqualTo("Resultado: 3 (discos)\n");
    }
}