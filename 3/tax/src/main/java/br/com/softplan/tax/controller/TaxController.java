package br.com.softplan.tax.controller;

import br.com.softplan.tax.dto.TaxRequest;
import br.com.softplan.tax.dto.TaxResponse;
import br.com.softplan.tax.service.TaxService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@AllArgsConstructor
public class TaxController{

    private TaxService service;

    @PostMapping("valueWithTaxes")
    public TaxResponse calculateTaxes(@Valid @RequestBody TaxRequest request) {
        return service.calculateTaxes(request);
    }

}
