package br.com.softplan.tax.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.math.BigDecimal;

@Getter
@AllArgsConstructor
public class TaxResponse {

    private BigDecimal valueWithTaxes;

}
