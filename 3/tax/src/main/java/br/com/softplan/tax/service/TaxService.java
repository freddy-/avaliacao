package br.com.softplan.tax.service;

import br.com.softplan.tax.dto.TaxRequest;
import br.com.softplan.tax.dto.TaxResponse;
import org.springframework.stereotype.Service;

import java.math.RoundingMode;

import static br.com.softplan.tax.utils.BigDecimalUtils.percentage;

@Service
public class TaxService {

    private static final int TWO_DIGITS_SCALE = 2;

    public TaxResponse calculateTaxes(TaxRequest request) {
        return new TaxResponse(
            request
                .getAmount()
                .add(percentage(request.getTax(), request.getAmount()))
                .setScale(TWO_DIGITS_SCALE, RoundingMode.CEILING));
    }
}
