package br.com.softplan.tax.utils;

import java.math.BigDecimal;

public class BigDecimalUtils {

    private static final BigDecimal ONE_HUNDRED = BigDecimal.valueOf(100);

    private BigDecimalUtils() {}

    public static BigDecimal percentage(BigDecimal percent, BigDecimal value) {
        return percent.divide(ONE_HUNDRED).multiply(value);
    }
}
