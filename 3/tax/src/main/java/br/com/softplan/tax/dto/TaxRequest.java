package br.com.softplan.tax.dto;

import lombok.*;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Getter
@EqualsAndHashCode()
@NoArgsConstructor
@AllArgsConstructor
public class TaxRequest {

    @NotNull(message = "Tax não pode ser vazio.")
    private BigDecimal tax = BigDecimal.ZERO;

    @NotNull(message = "Amount não pode ser vazio.")
    private BigDecimal amount = BigDecimal.ZERO;

}
