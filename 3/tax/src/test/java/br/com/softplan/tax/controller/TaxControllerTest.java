package br.com.softplan.tax.controller;

import br.com.softplan.tax.dto.TaxRequest;
import br.com.softplan.tax.dto.TaxResponse;
import br.com.softplan.tax.service.TaxService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class TaxControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private TaxService service;

    @Test
    public void valueWithTaxes_deveCalcularTaxas_quandoEnviadoParametrosValidos() throws Exception {
        when(service.calculateTaxes(eq(new TaxRequest(BigDecimal.valueOf(2.2), BigDecimal.valueOf(10000)))))
            .thenReturn(new TaxResponse(BigDecimal.valueOf(10220.00)));

        var requestBody = "{\"tax\": \"2.2\", \"amount\": \"10000\"}";

        mockMvc.perform(post("/valueWithTaxes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(requestBody))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.valueWithTaxes", is(10220.00)));
    }

    @Test
    public void valueWithTaxes_deveRetornarBadRequest_quandoCorpoDoPostForVazio() throws Exception {
        mockMvc.perform(post("/valueWithTaxes")
            .contentType(MediaType.APPLICATION_JSON))
            .andDo(print())
            .andExpect(status().isBadRequest());
    }
}