package br.com.softplan.tax.utils;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

class BigDecimalUtilsTest {

    @Test
    public void percentage_deveRetornarCalculadoCorretamente_quandoInformadosOsValores() {
        Assertions
            .assertThat(BigDecimalUtils.percentage(BigDecimal.ONE, BigDecimal.TEN))
            .isEqualByComparingTo(BigDecimal.valueOf(0.1));

        Assertions
            .assertThat(BigDecimalUtils.percentage(BigDecimal.ONE, BigDecimal.ZERO))
            .isEqualByComparingTo(BigDecimal.ZERO);

        Assertions
            .assertThat(BigDecimalUtils.percentage(BigDecimal.ZERO, BigDecimal.ONE))
            .isEqualByComparingTo(BigDecimal.ZERO);
    }

}