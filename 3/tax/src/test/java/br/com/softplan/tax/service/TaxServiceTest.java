package br.com.softplan.tax.service;

import br.com.softplan.tax.dto.TaxRequest;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

class TaxServiceTest {

    private TaxService service = new TaxService();

    @Test
    public void calculateTaxes_deveCalcularValorComTaxas_quandoInformadosOsParametros() {
        assertThat(service.calculateTaxes(new TaxRequest(BigDecimal.valueOf(100), BigDecimal.TEN)))
            .extracting("valueWithTaxes")
            .isEqualTo(BigDecimal.valueOf(20).setScale(2));

        assertThat(service.calculateTaxes(new TaxRequest(BigDecimal.valueOf(2.2), BigDecimal.valueOf(10000))))
            .extracting("valueWithTaxes")
            .isEqualTo(BigDecimal.valueOf(10220).setScale(2));

        assertThat(service.calculateTaxes(new TaxRequest()))
            .extracting("valueWithTaxes")
            .isEqualTo(BigDecimal.ZERO.setScale(2));
    }

}