Foi utilizado o JaCoCo para validar o coverage dos testes, ele foi configurado para que haja 100% de cobertura nos testes. 

A versão do Java utilizada no projeto é a 11.

Foi feito deploy desta API no Heroku, sendo possível acessar através da URL `https://exercicio-softplan.herokuapp.com/valueWithTaxes
`

Para validar a cobertura de testes basta executar o comando:
```
mvn verify
```
Será gerado o arquivo de reports:
```
target/site/jacoco/index.html
```

Exemplo de requisição:
```
$ curl -X POST \   
     -H "Content-Type: application/json" \
     -H "Accept: application/json" \
     -d '{"tax": "2.2", "amount": "10000"}' \
     http://localhost:8080/valueWithTaxes
```
O retorno será:
```
{"valueWithTaxes":10220.00}
```
